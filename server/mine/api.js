const Express = require('express');
const handlers = require('./handlers');

const API = Express();

API.get('/b/:width/:height', handlers.startNewGameHandler());

API.get('/r/:boardId/:x/:y', handlers.revealBoardTargetHandler());

module.exports = API;

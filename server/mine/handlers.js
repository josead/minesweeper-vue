const GL = require('./game-logic');

const startNewGameHandler = ({ width, height } = {}) => {
    return function StartNewGameHandler(req, res) {
        const options = {
            width: width || req.params.width < 100 ? req.params.width : 100,
            height: height || req.params.height < 100 ? req.params.height: 100,
        }

        // Init New Game Async
        GL.newGame(options).then(boardId => {
            res.status(200).send(boardId);
        }).catch(err => {
            res.status(500).send(`Error in server! ${err}`)
        });

    }
}

const revealBoardTargetHandler = (opt) => {
    return function RevealBoardTargetHandler(req, res) {
        const options = {
            boardId: req.params.boardId,
            x: req.params.x*1,
            y: req.params.y*1,
        }

        // Reveal boardId's block
        GL.reveal(options).then(value => {
            res.status(200).send(value);
        }).catch(err => {
            res.status(500).send(`Error in server! ${err}`)
        });
    }
}

module.exports = {
    startNewGameHandler,
    revealBoardTargetHandler,
}

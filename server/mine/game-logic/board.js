const shortid = require('shortid');

const BLOCK = {
    MINE: -1,
    FREE: 0,
}

const PROXIMITY = {
    0: 'U',
    1: 'UR',
    2: 'R',
    3: 'RD',
    4: 'D',
    5: 'DL',
    6: 'L',
    7: 'LU',
}

const generateZeroMatrix = (w, h) => {
    const arr = new Array(w);

    for (let i = 0; i < arr.length; i++) {
        arr[i] = new Array(h);
        for (let j = 0; j < arr[i].length; j++) {
            arr[i][j] = BLOCK.FREE;
        }
    }

    return arr;
}

const getProximityInfo = (matrix, x, y) => {
    let count = 0;
    const w = matrix.length-1;
    const h = matrix[0].length;

    for (let i = 0; i < 8; i++) {
        if ({
                U:  (m, x, y) => { return          y > 0 && m[x][y-1]; },
                UR: (m, x, y) => { return x < w && y > 0 && m[x+1][y-1]; },
                R:  (m, x, y) => { return x < w &&          m[x+1][y]; },
                RD: (m, x, y) => { return x < w && y < h && m[x+1][y+1]; },
                D:  (m, x, y) => { return          y < h && m[x][y+1]; },
                DL: (m, x, y) => { return x > 0 && y < h && m[x-1][y+1]; },
                L:  (m, x, y) => { return x > 0 &&          m[x-1][y]; },
                LU: (m, x, y) => { return x > 0 && y > 0 && m[x-1][y-1]; },
            }[PROXIMITY[i]](matrix, x, y) === BLOCK.MINE)
            count++;
    }
    return count;
}

const getRandomRange = (max) => {
    return Math.floor(Math.random() * Math.floor(max))
}

class Board {
    constructor(opt) {
        this.width = opt.width*1 || 20;
        this.height = opt.height*1 || 20;
        this.minesRate = opt.minesRate || 0.2;
        this.mines = Math.floor(this.width * this.height * this.minesRate);
        this.matrix = this.generateBoardMatrix();
        this.revealedMatrix = generateZeroMatrix(this.width, this.height);
        this.revealedCount = 0;
        this.id = shortid.generate();
    }

    reveal(x, y) {
        this.revealedCount++;
        this.revealedMatrix[x][y] = 1;
    }

    isRevealed(x, y) {
        return this.revealedMatrix[x][y] === 1;
    }

    spread(x, y) {
        if (x < 0 ||
            x > this.width-1 ||
            y < 0 ||
            y > this.height-1 ||
            this.revealedMatrix[x][y] === 1) return;
        this.reveal(x, y);
        if (this.matrix[x][y] === BLOCK.FREE) {
            for (let i = 0; i < 4; i++) {
                ({
                    U:  () => this.spread(x, y-1),
                    UR: () => this.spread(x+1, y-1),
                    R:  () => this.spread(x+1, y),
                    RD: () => this.spread(x+1, y+1),
                    D:  () => this.spread(x, y+1),
                    DL: () => this.spread(x-1, y+1),
                    L:  () => this.spread(x-1, y),
                    LU: () => this.spread(x-1, y-1),
                })[PROXIMITY[i*2]]();
            }
        }
    }

    generateBoardMatrix() {
        const matrix = generateZeroMatrix(this.width, this.height);

        // Placing Mines Randomly
        let m = this.mines;

        while (m > 0) {
            let randomX = getRandomRange(this.width);
            let randomY = getRandomRange(this.height);
            if (matrix[randomX][randomY] !== BLOCK.MINE) {
                matrix[randomX][randomY] = BLOCK.MINE;
                m--;
            }
        }

        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                if (matrix[i][j] !== BLOCK.MINE) {
                    matrix[i][j] = getProximityInfo(matrix, i, j);
                }
            }
        }

        return matrix;
    }

    minesCount() {
        return this.mines;
    }

    blocksCount() {
        return this.width * this.height;
    }

    isMine(x, y) {
        return this.matrix[x][y] === BLOCK.MINE;
    }

    isZero(x, y) {
        return this.matrix[x][y] === BLOCK.FREE;
    }

    getId() {
        return this.id;
    }

    getProximityInfo(x, y) {
        return this.matrix[x][y];
    }

    getRevealedCount() {
        return this.revealedCount;
    }

    getRevealed() {
        const revealed = [];

        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                if (this.revealedMatrix[i][j] === 1) {
                    revealed.push({
                        x: i,
                        y: j,
                        value: this.matrix[i][j],
                    });
                }
            }
        }
        
        return revealed;
    }

    dispose() {
        console.log(`BOARD: ${this.getId()} - removed.`)
    }
}


module.exports = Board;
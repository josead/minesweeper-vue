const state = require('./state');

const newGame = function NewGame(options) {
    return state.newGameState({
        board: options,
    }).then(gameState => {
        return gameState.getBoardId();
    });    
}

const reveal = function Reveal(options) {
    return state
        .getGameState(options.boardId)
        .then(gameState => {
            return gameState.reveal(options.x, options.y);
        })
}

module.exports = {
    newGame,
    reveal,
}
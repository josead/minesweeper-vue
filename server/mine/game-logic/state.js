const Board = require('./board');

// States persistence in memory
const gameStatesRunning = new Map();

/*
* State
*   board: game randomly generated board
*   revealGoal: calculated ammount of blocks to be revealed to win the game
*/
class State {
    constructor(opt) {
        this.board = new Board(opt.board);
        this.revealGoal = this.board.blocksCount() - this.board.minesCount();
    }

    getBoardId() {
        return this.board.getId();
    }

    reveal(x, y) {
        if (this.board.isRevealed(x, y)) return this.continue();
        if (this.board.isMine(x, y)) return this.end(false);
        
        const isZero = this.board.isZero(x, y);
        
        if (isZero) {
            this.board.spread(x, y);
            
        } else {
            // has proximity info
            this.board.reveal(x, y);    
        }

        const reveal =  isZero ? this.board.getRevealed() : [{ x, y, value: this.board.getProximityInfo(x, y)}];
        
        if (this.board.getRevealedCount() === this.revealGoal) {
            return this.end(true, reveal);
        }
        return this.continue(reveal);
    }

    end(isWin, revealed) {
        // Delete all board and state records.
        this.board.dispose();
        gameStatesRunning.delete(this.board.getId());
        return {
            gameState: isWin ? 'w' : 'l',
            revealed,
        };
    }

    continue(revealed) {
        return {
            gameState: 'c',
            revealed,
        };
    }
}

const newGameState = function NewGameState(opt) {
    // Promise in case gameState needs persistence in db.
    return new Promise((res, rej) => {
        const gs = new State(opt);
        
        // Save State with boardId
        gameStatesRunning.set(gs.getBoardId(), gs);

        res(gs);
    });
}

const getGameState = function GetGameState(boardId) {
    return new Promise((res, rej) => {
        const board = gameStatesRunning.get(boardId)
        if (board) res(board);
        else rej('Game not found.');
    });
}

module.exports = {
    State,
    newGameState,
    getGameState,
}
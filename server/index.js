const Express = require('express');
const App = require('./app');

const port = process.env.port || 8000;
const server = Express();

App(server);

server.listen(port, () => console.log(`API on ${port}`));

const Express = require('express');

const MineAPI = require('./mine/api');


module.exports = function InitializeApp(server) {

    // Init DB here

    server.use('/api', MineAPI);

    server.use('/', Express.static('build'));
}

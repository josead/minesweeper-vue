import { START_NEW_GAME, REVEAL_BLOCK } from './actions.type';
import { FETCH_START, FETCH_END, SET_REVEALED, APPLY_MATRIX, LOSE_GAME } from './mutations.type';
import ApiService from '../common/api.service';
import { GAME_BLOCK, GAME_DEFAULT_WIDTH, GAME_DEFAULT_HEIGHT } from '../common/config';

const generateMatrix = (width, height) => {

    const matrix = new Array(width);

    for (let i = 0; i < width; i++) {
        matrix[i] = new Array(height);
        for (let j = 0; j < height; j++) {
            matrix[i][j] = {
                value: null,
                x: i,
                y: j,
            };                
        }
    }
    
    return matrix;
}

const state = {
    id: null,
    revealed: 0,
    matrix: [],
    isEnd: false,
    isLoading: false,
};

const getters = {
    revealed(state) {
        return state.revealed;
    },
    matrix(state) {
        return state.matrix;
    },
    blocks(state) {
        return state.matrix.reduce((a, b) => a.concat(b), []);
    },
    isLoading(state) {
        return state.isLoading;
    },
    id(state) {
        return state.id;
    },
    isEnd(state) {
        return state.isEnd;
    },
};

const actions = {
    [START_NEW_GAME]({ commit }) {
        commit(FETCH_START);
        return ApiService.newGame()
            .then(({ data }) => {
                commit(FETCH_END, data);
            })
            .catch(error => {
                throw new Error(error);
            });
    },
    [REVEAL_BLOCK]({ commit, state }, { x, y }) {
        return ApiService.revealBlock(state.id, x, y)
            .then(({ data }) => {
                if (data.revealed) {
                    data.revealed.map((block) => {
                        commit(APPLY_MATRIX, { x: block.x, y: block.y, value: block.value });
                    });
                    commit(SET_REVEALED, data.revealed.length);
                }
                if (data.gameState === 'w') {
                    commit(WIN_GAME);
                    console.log('All mines detected! Congratulations!');
                } else if (data.gameState === 'l') {
                    commit(APPLY_MATRIX, { x, y, value: GAME_BLOCK.MINE });
                    commit(LOSE_GAME);
                    console.log('You lose!');
                }
            });
    }
};

const mutations = {
    [FETCH_START](state) {
        state.isLoading = true;
    },
    [FETCH_END](state, gameId) {
        state.id = gameId;
        state.isEnd = false;
        state.isLoading = false;
        state.matrix = generateMatrix(GAME_DEFAULT_WIDTH, GAME_DEFAULT_HEIGHT);
    },
    [SET_REVEALED](state, revealedCount) {
        state.revealed = state.revealed+revealedCount;
    },
    [APPLY_MATRIX](state, { x, y, value }) {
        state.matrix[x][y].value = value;
    },
    [LOSE_GAME](state) {
        state.isEnd = true;
    }
};
  

export default {
    state,
    getters,
    actions,
    mutations,
};
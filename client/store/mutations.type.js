export const FETCH_END = 'setGameId';
export const FETCH_START = 'setLoading';
export const SET_REVEALED = 'setRevealed';
export const APPLY_MATRIX = 'applyMatrix';
export const WIN_GAME = 'gameEndWinning';
export const LOSE_GAME = 'gameEndLosing';

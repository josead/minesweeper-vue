export const API_URL = 'localhost:8000';

export const GAME_DEFAULT_WIDTH = 5;

export const GAME_DEFAULT_HEIGHT = 5;

export const GAME_BLOCK = {
    MINE: -1,
    FREE: 0,
    ONE: 1,
    TWO: 2,
    TREE: 3,
    FOUR: 4,
    FIVE: 5,
    SIX: 6,
    SEVEN: 7,
    EIGHT: 8,
};

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import { API_URL, GAME_DEFAULT_HEIGHT, GAME_DEFAULT_WIDTH } from './config';

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
    },

    newGame() {
        return Vue.axios.get(`/api/b/${GAME_DEFAULT_WIDTH}/${GAME_DEFAULT_HEIGHT}`)
            .catch(error => {
                throw new Error(`[ApiService] newGame: ${error}`);
            });
    },

    revealBlock(gameId, x, y) {
        return Vue.axios.get(`/api/r/${gameId}/${x}/${y}`)
            .catch(error => {
                throw new Error(`[ApiService] revealBlock: ${error}`);
            });
    }
};

export default ApiService;

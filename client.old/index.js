import React from 'react';
import ReactDOM from 'react-dom';

import Mine from './mine.js' 

ReactDOM.render(
  <Mine />,
  document.getElementById('app')
);

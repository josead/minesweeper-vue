import React from 'react';
import styled from 'styled-components';
import { BLOCK } from '../mine/constants';

const getColorByType = (type) => {
    const colors = {};

    colors[BLOCK.FREE] = '#CCC';
    colors[BLOCK.MINE] = '#E55';

    return colors[type];
}

const Block = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: ${props => props.type === null ? 'pointer' : 'default'};
    border: 1px solid #CCC;
    background: ${props => getColorByType(props.type)};
`;

export default Block;
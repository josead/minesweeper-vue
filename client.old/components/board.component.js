import React from 'react';
import styled from 'styled-components';
import shortid from 'shortid';

import Block from './block.component';

const createGridTemplateColumns = (count) => {
    let template = '';
    for (let i = 0; i < count; i++) {
        template += '1fr '
    }
    return template;
}

const Proximity = styled.span`
    font-size: 3rem;
`;

const generateBlocks = (matrix, onBlockClick) => {
    return matrix.map((column, xIndex) => {
        return column.map((cellValue, yIndex) => (
            <Block
                key={`${xIndex}-${yIndex}`}
                type={cellValue}
                onClick={onBlockClick.bind(null, cellValue, xIndex, yIndex)}>
                <Proximity>{cellValue > 0 ? cellValue : ''}</Proximity>
            </Block>
        ));
    });
}

const BoardGrid = styled.div`
    position: relative;
    height: 100%;
    padding: 1rem;
    display: grid;
    grid-template-columns: ${props => createGridTemplateColumns(props.width)};
    grid-template-rows: ${props => createGridTemplateColumns(props.height)};
`;

const Backdrop = styled.div`
    position: absolute;
    top: 0; left: 0; right: 0; bottom: 0;
    background: #000;
    opacity: 0.6;
    z-index: 999;
    display: ${({ show }) => show ? 'block' : 'none'};
`

const Board = ({ disabled, matrix, onReveal }) => {
    if (matrix && matrix[0]) {
        return [
            <BoardGrid width={matrix.length} height={matrix[0].length}>
                <Backdrop show={disabled} />
                {generateBlocks(matrix, onReveal)}
            </BoardGrid>
        ];
    }
    return ("Board is loading...");
}


export default Board;

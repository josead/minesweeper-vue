export const DEFAULT_WIDTH = 10;

export const DEFAULT_HEIGHT = 10;

export const BLOCK = {
    MINE: -1,
    FREE: 0,
};

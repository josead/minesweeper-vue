import React from 'react';
import Axios from 'axios';

import { DEFAULT_WIDTH, DEFAULT_HEIGHT, BLOCK } from '../mine/constants';

import Board from '../components/board.component';

class BoardContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            revealed: 0,
            matrix: [],
            id: props.id,
        };

        this.reveal = this.reveal.bind(this);
    }

    generateMatrix(width, height) {
        const matrix = new Array(width);

        for (let i = 0; i < width; i++) {
            matrix[i] = new Array(height);
            for (let j = 0; j < height; j++) {
                matrix[i][j] = null;                
            }
        }

        return matrix;
    }

    resetMatrix() {
        this.setState(() => ({
            matrix: this.generateMatrix(DEFAULT_WIDTH, DEFAULT_HEIGHT),
        }))
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.id !== this.props.id) {
            this.resetMatrix();
        }
    }

    applyMatrix(x, y, value) {
        this.setState((oldState) => {
            oldState.matrix[x][y] = value ? value : BLOCK.FREE;
            return {
                matrix: oldState.matrix,
            }
        })
    }

    reveal(value, x, y) {
        if (value !== null) return;
        console.log('Clicked ', x, y);
        Axios.get(`/api/r/${this.props.id}/${x}/${y}`).then(({ data }) => {
            if (data.revealed) data.revealed.map((data) => {
                this.applyMatrix(data.x, data.y, data.value)
            });
            if (data.gameState === 'w') {
                this.props.onWin();
                console.log('All mines detected! Congratulations!');
            } else if (data.gameState === 'l') {
                this.applyMatrix(x, y, BLOCK.MINE );
                this.props.onLose();
                return console.log('You lose!');
            }
        });
    }

    render() {
        return (
            <Board
                disabled={this.props.isEnd}
                matrix={this.state.matrix}
                onReveal={this.reveal}
                />
        );
    }
}

export default BoardContainer;

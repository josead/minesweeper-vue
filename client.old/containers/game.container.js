import React from 'react';
import Axios from 'axios';

import Button from '../components/button.component';

import BoardContainer from './board.container';

import { DEFAULT_HEIGHT, DEFAULT_WIDTH } from '../mine/constants';

class GameContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEnd: false,
        };

        this.startNewGame = this.startNewGame.bind(this);
        this.winGame = this.winGame.bind(this);
        this.loseGame = this.loseGame.bind(this);
    }

    componentDidMount() {
        this.startNewGame();
    }

    startNewGame() {
        Axios.get(`/api/b/${DEFAULT_WIDTH}/${DEFAULT_HEIGHT}`)
            .then(({ data }) => {
                this.setState((oldState) => ({
                    boardId: data,
                    isEnd: false,
                }));
            });
    }

    winGame() {
        this.setState(() => ({
            isEnd: 'WIN',
        }));
    }
    
    loseGame() {
        this.setState(() => ({
            isEnd: 'LOSE',
        }));
    }

    render() {
        return [
            <Button onClick={this.startNewGame}>New Game</Button>,
            <BoardContainer
                id={this.state.boardId}
                onWin={this.winGame}
                onLose={this.loseGame}
                isEnd={this.state.isEnd}
                />
        ];
    }
}

export default GameContainer;

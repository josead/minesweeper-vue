import React from 'react';

import GameContainer from './containers/game.container';

import './styles/vendor/normalize.css';
import './styles/common.css';

const Mine = (props) => (
    <GameContainer />
);

export default Mine;
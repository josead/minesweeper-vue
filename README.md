## Minesweeper 

After reading the game's basic rules and logic, I've decided to do a list of modules tobe implemented covering features of the game.

- GameState: Responsible of maintaining the game metadata and player's input

- GameBoard: Responsible of the matrix and its manipulation


    > Matrix Legend
    >
    > [
    >
    >   [ 1,-1, 1, 0], 
    >
    >   [ 2, 2, 1, 0],
    >
    >   [-1, 1, 0, 0],
    >
    >   [ 1, 1, 0, 0]
    >
    > ]
    >
    >  0: Free space
    >
    >  -1: Mine
    >
    >  1...8: Ammount of mines near

    - GenerateBoard({ width, height: number }): Board {}

    - RanomizeMatrix(matrix: Array<Array<number>>): Array<Array<number>> {}

    - AddProximityInfo(matrix: Array<Array<number>>): Array<Array<number>> {}

    - RevealMatrix(matrix: Array<Array<number>, x, y: number)

### Implementation Descitions

Matrix: To keep it simple I've decided to use an array of arrays.

StartGame: First block will not be protected.

## NODE API

The first version of the API should look like this:


`/api/b|board/[<w>/<h>]` -> StartNewGameHandler, returns a new `board_id`, with default `width` and `height` if not specified.

`/api/r|reveal/<board_id>/x,y` -> RevealBoardTargetHandler, this returns 3 possible states: win, lose or continue. x and y start from 0.

### Implementation Descitions

The API ought to be minimal, to support mobile players with poor network capabilities.
All load will be put on BE, to simulate a real unknown state of the game, meaning that the state will remain in the server. Meanwhile the Front End will send minimized instructions regarding the current game manipulation.
To keep improving for mobile games, when the webapp is retrieved, a `board_id` will be sent to start a game inmediately.

## React App

For the sake of simplicity I've installed a very minimal set of tools to make the webapp with my favourite front-end stack.
Webpack as the bundler, also Babel.

## Stack
- Node.js v8.11.3 (LTS at this moment)
- Express.js
- Functional Programming Approach
- Mocha

- React.js
- Styled-components
- Babel
- Css
